const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: './src/js/app.js',
  output: {
    path: path.resolve(__dirname, 'public/js'),
    filename: 'bundle.js',
  },
  module: {
    rules: [{
      test: /\.(css|sass|scss)$/,
      use: [
        'style-loader',
        'css-loader',
        'sass-loader',
        {
          loader: 'postcss-loader',
          options: {
            plugins: function () {
              return [require('autoprefixer')];
            }
          }
        }
      ]
    },{
      test: /\.(jpe?g|png|gif|svg|ico)(\?.+)?$/,
        use: {
          loader: 'url-loader',
          // options: {
          //     limit: 10000,
          //     name: './img/[name].[ext]'
          // }
        }
    },{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }],
    }],
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery'
    }),
  ],
  devServer: {
    contentBase: path.resolve(__dirname, 'public'),
    publicPath: '/js/',
    watchContentBase: true
  },
};