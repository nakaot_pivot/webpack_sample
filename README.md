## 概要

* js,cssを一つにまとめて出力
* 開発環境ではdevサーバーが起動、livereload有り
* 納品用コマンド有り（納品時はコードを圧縮）

## 推奨環境

node 6.10.3以上推奨

## ディレクトリ構造

webpack_sample  
	├ node_modules … node　module入れ（git非管理）  
	├ package.json … インストールするパッケージリスト  
	├ public … 納品用ファイル出力先（html,html内で使用するimgはこの中で直接管理）  
	└ src … 開発用ディレクトリ（ES6,sass対応）  
	
	
## セットアップ

clone後、ブロジェクトディレクトリ直下に移動し、下記のコマンドを実行

```
npm install
```

## コマンド

開発用

```
npm strat
```

納品用

```
npn run release
```

## 特記事項

#### 画像の保存方法
今回、一部画像をbase64への変換を行う為、画像の用途によって保存先が異なります。

* HTMLのimgタグ等で直接画像を使用する場合  
	public内のimgディレクトリ内に直接保存

* css内で使用する画像の場合  
	src内のimgディレクトリ内に直接保存  
	css内で呼び出された画像はbase64に変換されて呼び出されます。  